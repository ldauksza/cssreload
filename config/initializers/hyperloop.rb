# config/initializers/hyperloop.rb
# If you are not using ActionCable, see http://ruby-hyperloop.io/docs/models/configuring-transport/
Hyperloop.configuration do |config|
  #config.transport = :action_cable

  config.transport = :action_cable

  #config.import 'client_and_server'
  #config.import 'client_only', client_only: true

  config.import Webpacker.manifest.lookup("client_and_server.js").split("/").last
  config.import Webpacker.manifest.lookup("client_only.js").split("/").last

  config.import 'opal_hot_reloader'

  config.console_auto_start = false
end

